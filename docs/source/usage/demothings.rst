Show embedded resources
=======================

Include an image
----------------

.. image:: /images/axual.png

Download
--------

:download:`Download the image above: </images/axual.png>`

Include a video
---------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/vb0LUWptvWc?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>