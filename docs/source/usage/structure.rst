
*******************
Structural Elements
*******************

.. contents:: Table of Contents

The Knights Who Say Ni demand a sacrifice! Bring her forward! We shall say 'Ni' again to you, if you do not appease us.
You don't frighten us, English pig-dogs! Go and boil your bottoms, sons of a silly person! I blow my nose at you,
so-called Ah-thoor Keeng, you and all your silly English K-n-n-n-n-n-n-n-niggits!

---------

And the hat. She's a witch! Why do you think that she is a witch? No, no, no! Yes, yes. A bit. But she's got a wart.
Where'd you get the coconuts?

Document Section
================

The Knights Who Say Ni demand a sacrifice! I don't want to talk to you no more, you empty-headed animal food trough
water! I fart in your general direction! Your mother was a hamster and your father smelt of elderberries! Now leave
before I am forced to taunt you a second time!

Document Subsection
-------------------

Oh! Come and see the violence inherent in the system! Help, help, I'm being repressed! You can't expect to wield supreme
power just 'cause some watery tart threw a sword at you! You can't expect to wield supreme power just 'cause some watery
tart threw a sword at you!

- She looks like one.
- Burn her anyway!
- The nose?

Document Subsubsection
^^^^^^^^^^^^^^^^^^^^^^

The Lady of the Lake, her arm clad in the purest shimmering samite, held aloft Excalibur from the bosom of the water,
signifying by divine providence that I, Arthur, was to carry Excalibur. That is why I am your king. What a strange
person.

Document Paragraph
""""""""""""""""""

Found them? In Mercia?! The coconut's tropical! We want a shrubbery!! I don't want to talk to you no more, you
empty-headed animal food trough water! I fart in your general direction! Your mother was a hamster and your father
smelt of elderberries! Now leave before I am forced to taunt you a second time!

*********************
Structural Elements 2
*********************

Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's
nest nipperkin grog yardarm hempen halter furl. Swab barque interloper chantey doubloon starboard grog black jack
gangway rutters.

Document Section
================

Deadlights jack lad schooner scallywag dance the hempen jig carouser broadside cable strike colors. Bring a spring upon
her cable holystone blow the man down spanker Shiver me timbers to go on account lookout wherry doubloon chase. Belay
yo-ho-ho keelhaul squiffy black spot yardarm spyglass sheet transom heave to.

Document Subsection
-------------------

.. figure:: /images/yi_jing_01_chien.jpg
    :align: right
    :figwidth: 200px

    This is a caption for a figure. Text should wrap around the caption.

Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis.
Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis
soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated
corpse, cricket bat max brucks terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat mortuos comedere
carnem virus. Zonbi tattered for solum oculi eorum defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut
malus putrid voodoo horror. Nigh tofth eliv ingdead.
Cum horribilem walking dead resurgere de crazed sepulcris creaturis, zombie sicut de grave feeding iride et serpens.
Pestilentia, shaun ofthe dead scythe animated corpses ipsa screams. Pestilentia est plague haec decaying ambulabat
mortuos. Sicut zeder apathetic malus voodoo. Aenean a dolor plan et terror soulless vulnerum contagium accedunt, mortui
iam vivam unlife. Qui tardius moveri, brid eof reanimator sed in magna copia sint terribiles undeath legionis. Alii
missing oculis aliorum sicut serpere crabs nostram. Putridi braindead odores kill and infect, aere implent left four
dead.

