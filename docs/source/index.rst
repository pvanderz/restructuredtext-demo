.. Axual Platform documentation master file, created by
   sphinx-quickstart on Wed Aug 29 13:15:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Axual Platform's documentation!
==========================================

You are currenctly looking at version 3.0.0. This is to show that versions can coexist!

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/structure
   usage/demo
   usage/lists-tables
   usage/demothings
   gettingstarted/inburgering


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
