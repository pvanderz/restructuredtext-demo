Axual Inburgering
=================

Walkthrough-AXDEV
-----------------

Creation of dev-tools's users for AXUAL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

GMail
_____

.. attention::
   username: <your-name>@axual.io

Atlassian Group
_______________

.. caution::
   Link URL: https://axualio.atlassian.net

Log in with your GMail Axual's Account
 #. Confluence – `Confluence HomePage <https://axualio.atlassian.net/wiki/discover/all-updates>`_
 #. Jira – `JIRA HomePage <https://axualio.atlassian.net/secure/Dashboard.jspa>`_

Gitlab
______

.. danger::
   Link URL: https://gitlab.com

Create an account using your Axual Account, then ask Abhinav Sonkar to add you inside the relatives projects.

Sonatype Nexus
______________

.. error::
   Link URL: https://dev.axual.io/nexus/#browse/browse

Create an account using your Axual Account.

Slack
_____

.. hint::
   Link URL: https://axual.slack.com

Log with your Axual Account, if you are not able to see the Axual's workspace,
please ask Joris Meijer to be add to the group.

BeeBole
_______

.. important::
   Link URL: https://beebole-apps.com/#home

Log with your Axual Account, if you are not able to see the Axual's timesheet,
please ask Jeroen van Disseldorp to be add to the group.

Setting local environment for developing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

JDK 8
_____

.. note::
   Link URL: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

After install JDK, check the effective installation by typing 'java -version' on shell:

JDK info

.. code-block:: console

   $ java -version
   java version "1.8.0_161"
   Java(TM) SE Runtime Environment (build 1.8.0_161-b12)
   Java HotSpot(TM) 64-Bit Server VM (build 25.161-b12, mixed mode)

Maven
_____

.. tip::
   Link URL: https://maven.apache.org/index.html

Download and install maven on your machine, check the effective installation by typing 'mvn -version' on shell:

Maven Info

.. code-block:: console

   $ mvn -version
   Apache Maven 3.5.3 (3383c37e1f9e9b3bc3df5050c29c8aff9f295297; 2018-02-24T20:49:05+01:00)
   Maven home: /usr/local/Cellar/maven/3.5.3/libexec
   Java version: 1.8.0_161, vendor: Oracle Corporation
   Java home: /Library/Java/JavaVirtualMachines/jdk1.8.0_161.jdk/Contents/Home/jre
   Default locale: en_NL, platform encoding: UTF-8
   OS name: "mac os x", version: "10.13.4", arch: "x86_64", family: "mac"

Setting.xml
___________

This file let you access the maven repository in ours Cloud (or in some cases the one of Rabobank)
settings_dev_base.xml

Docker Community Edition
________________________

Download Docker for Mac

Install docker for Mac by following the instructions https://docs.docker.com/docker-for-mac/install/

Check the proper installation by typing ``'docker --version'`` on shell:

Docker Info

.. code-block:: console
   :emphasize-lines: 1

   $ docker --version
   Docker version 18.03.0-ce, build 0520e24

Run 'setup.sh', it creates an alias address for loopback; Docker need it

Save 'host.sh' on your home folder; Axual-Deploy need it.

IntelliJ IDEA
_____________

Download IntelliJ IDEA, ask Joris Meijer for a commercial license.

Source Tree (GitUI)
___________________

Download SourceTree, you will use it to synchronising your working directory and remote working directory

AXUAL DEPLOY – AXUAL CLIENT EXAMPLE
-----------------------------------
**Deploy Axual platform to your machine.**

#. Open GitLab and copy the link of axual-deploy
#. Open SourceTree, click new → clone from URL → fill the empty texts with your workspace paths
#. Checkout the develop branch
#. Open axual-deploy on IntelliJ IDEA

   #. Explanation of the project's scaffolding (TODO)
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fermentum vestibulum est. Cras rhoncus.
      Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed quis tortor.
      Donec non ipsum. Mauris condimentum, odio nec porta tristique, ante neque malesuada massa, in dignissim eros
      velit at tellus. Donec et risus in ligula eleifend consectetur. Donec volutpat eleifend augue. Integer gravida
      sodales leo. Nunc vehicula neque ac erat. Vivamus non nisl. Fusce ac magna. Suspendisse euismod libero eget
      mauris.

#. Open a terminal and navigate to the root folder of axual-deploy
#. Run './axual.sh start' , this let you start all the axual platform.

   .. code-block:: console
      :caption: Start Axual
      :emphasize-lines: 1

         $ ./axual.sh start
         Configuring all system services for 192.168.99.100 in LOCAL
         Starting data: Done
         Starting exhibitor: Done
         Waiting for exhibitor on 192.168.99.100
         Waiting for exhibitor on 192.168.99.100
         Waiting for exhibitor on 192.168.99.100
         Waiting for exhibitor on 192.168.99.100
         Waiting for exhibitor on 192.168.99.100
         Waiting for exhibitor on 192.168.99.100
         Waiting for exhibitor on 192.168.99.100
         Waiting for exhibitor on 192.168.99.100
         Waiting for exhibitor on 192.168.99.100
         Connected to exhibitor on 192.168.99.100
         Starting broker: Done
         Running init-topics: /home/kafka/start-jar.sh: line 66: ll: command not found
         Done
         Starting sr-master: Done
         Starting sr-slave: Done
         Starting ccp: Done
         Starting admin: Done

#. Now, you can check the status of your Docker for understanding which containers are running

   .. code-block:: console
      :caption: Docker Containers Info
      :emphasize-lines: 1

         $ docker ps
         CONTAINER ID        IMAGE                                  COMMAND                  CREATED             STATUS              PORTS                                                                                            NAMES
         ef725c1beeb4        axual/admin:0.9.3                      "/home/kafka/start-j…"   22 hours ago        Up 22 hours         0.0.0.0:9080->9080/tcp                                                                           admin
         1949c377b663        axual/ccp:1.2.6                        "/home/kafka/start-c…"   22 hours ago        Up 22 hours         0.0.0.0:8080->80/tcp, 0.0.0.0:8443->443/tcp                                                      ccp
         9998f59f9793        axual/schemaregistry:3.3.0             "/home/kafka/start-s…"   22 hours ago        Up 22 hours         0.0.0.0:8081->8081/tcp, 0.0.0.0:9003->9003/tcp                                                   sr-slave
         fa9afb371661        axual/schemaregistry:3.3.0             "/home/kafka/start-s…"   22 hours ago        Up 22 hours         127.0.0.1:8084->8084/tcp, 0.0.0.0:9002->9002/tcp                                                 sr-master
         74a7881b4377        axual/broker:feature-build-from-jdk8   "/home/kafka/start-b…"   22 hours ago        Up 22 hours         0.0.0.0:9001->9001/tcp, 0.0.0.0:9093->9093/tcp, 0.0.0.0:19093->8088/tcp                          broker
         070555495b61        axual/exhibitor:0.1.2                  "/home/kafka/start-e…"   22 hours ago        Up 22 hours         0.0.0.0:2181->2181/tcp, 0.0.0.0:2888->2888/tcp, 0.0.0.0:3888->3888/tcp, 0.0.0.0:8082->8082/tcp   exhibitor
         cf103be22aff        axual/data:0.4.4                       "/sbin/my_init"          22 hours ago        Up 22 hours                                                                                                          data

#. Your Admin is running so you can access Admin Axual API, in that page, /topics, its show all the available topics.

   .. warning::
      It's possible that at the first try it doesn't work, check the file  version.sh under ./system/business-event-bus/instance/local/  to change the number of the versions that need to be download from the maven repository.

      Now we gonna apply for a topic to be used by ours Producer/Consumer examples.
