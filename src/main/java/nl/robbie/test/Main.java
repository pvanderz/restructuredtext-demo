package nl.robbie.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Main {

    @Autowired
    public ApplicationDemolisher applicationDemolisher;

    public static void main(String... args) {
        SpringApplication.run(Main.class, args);
    }
}

@Component
class ApplicationDemolisher {

    @Autowired
    private ApplicationContext context;

    @PostConstruct
    public void destruct() {
        System.exit(SpringApplication.exit(context, () -> 45));
    }
}
