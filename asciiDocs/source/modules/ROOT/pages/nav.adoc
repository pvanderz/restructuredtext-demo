* Axual demo page
** xref:gettingstarted/inburgering.adoc[Axual Inburgering]
* Standard Demo files
** xref:usage/structure.adoc[Structural elements]
** xref:usage/demo.adoc[Just a Demo]
** xref:usage/lists-tables.adoc[Demo of tables and lists]
** xref:usage/demothings.adoc[Another demo thing]