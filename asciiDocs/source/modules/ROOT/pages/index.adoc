// Axual Platform documentation master file, created by
// sphinx-quickstart on Wed Aug 29 13:15:15 2018.
// You can adapt this file completely to your liking, but it should at least
// contain the root `toctree` directive.

= Welcome to Axual Platform's documentation!
Doc Writer <paul@axual.io>
:appversion: v1.0.0
:icons: font
date 2018-09-05

:toc: left

You are currently looking at version {appversion} in asciidoc. This is to show that versions can coexist!

include::usage/structure.adoc[]

include::usage/demo.adoc[]

include::usage/lists-tables.adoc[]

include::usage/demothings.adoc[]

include::gettingstarted/inburgering.adoc[]



